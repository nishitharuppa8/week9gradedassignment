package com.hcl.bookess.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.bookess.bean.Users;
import com.hcl.bookess.repository.UsersRepository;

@Service
public class UsersService {
	@Autowired
	UsersRepository usersDao;

	public String storeUserDetails(Users users) {
		if (!usersDao.existsById(users.getEmail())) {
			usersDao.save(users);
			return "Details saved Sucessfully";
		} else {
			return "Already details exists";
		}
	}

	public String updateUserDetails(Users users) {
		if (usersDao.existsById(users.getEmail())) {
			Users user = usersDao.getById(users.getEmail());
			if (user.getPassword().equals(users.getPassword())) {
				return "You are Providing the Old Input";
			} else {
				user.setPassword(users.getPassword());
				usersDao.saveAndFlush(user);
				return "Successfully Details are Updated";
			}
		} else {
			return "Invalid Username or Password";
		}
	}

	public List<Users> getAllUsersDetails() {
		return usersDao.findAll();
	}

	public String deleteUserDetails(String email) {
		if (!usersDao.existsById(email)) {
			return "Invalid Username or Password";
		} else {
			usersDao.deleteById(email);
			return "User Details are Deleted ";
		}
	}

}
