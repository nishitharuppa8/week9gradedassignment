package com.hcl.bookess.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.bookess.bean.Admin;
import com.hcl.bookess.repository.AdminRepository;

@Service
public class AdminService {
	@Autowired
	AdminRepository adminDao;

	public String adminRegistration(Admin admin) {
		if (adminDao.existsById(admin.getEmail())) {
			return "Already Details Exists";
		} else {
			adminDao.save(admin);
			return "Successfully details are Saved";
		}
	}

	public String checkLogin(Admin admin) {
		if (adminDao.existsById(admin.getEmail())) {
			Admin a = adminDao.getById(admin.getEmail());
			if (a.getPassword().equals(admin.getPassword())) {
				return "login sucessfull";
			} else {
				return "Give Proper Details";
			}
		} else {
			return "Your Details are not Available ";
		}

	}

	public List<Admin> getAllAdminAvaliable() {
		return adminDao.findAll();
	}

	public String logoutAdmin(String email) {
		if (!adminDao.existsById(email)) {
			return "Details are not Valid ";
		} else {
			adminDao.deleteById(email);
			return "Successfully Logged Out";
		}
	}

}
