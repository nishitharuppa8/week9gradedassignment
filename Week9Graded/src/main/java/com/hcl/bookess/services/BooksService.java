package com.hcl.bookess.services;
import java.util.List;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.bookess.bean.Books;
import com.hcl.bookess.repository.BooksRepository;

@Service
public class BooksService {
	
	@Autowired
	BooksRepository booksDao;
	
	public List<Books> getAllBooks() {
		return booksDao.findAll();
	}

	public Optional<Books> getBookById(int bid ) {
		return booksDao.findById(bid);
	}
	
	public String storeBook(Books book) {

		if (booksDao.existsById(book.getBookId())) {
			return "Book Id must be Unique";
		} else {
			booksDao.save(book);
			return "Book Added Successfully";
		}
	}
	
	public String updateBookName(Books book) {
		if (!booksDao.existsById(book.getBookId())) {
			return "Not Found";
		} else {
			Books b = booksDao.getById(book.getBookId());
			b.setTitle(book.getTitle());
			booksDao.saveAndFlush(b);
			return "Book is Updated ";
		}
	}
	
	public String deleteBook(int bookId) {
		if (!booksDao.existsById(bookId)) {
			return "Book  Details are not there";
		} else {
			booksDao.deleteById(bookId);
			return "Book is Deleted ";
		}
	}
}

