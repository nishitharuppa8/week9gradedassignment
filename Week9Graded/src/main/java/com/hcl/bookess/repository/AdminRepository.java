package com.hcl.bookess.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.bookess.bean.Admin;

@Repository
public interface AdminRepository extends JpaRepository<Admin, String> {

}

