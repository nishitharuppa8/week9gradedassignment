package com.hcl.bookess.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.bookess.bean.Books;

@Repository
public interface BooksRepository extends JpaRepository<Books, Integer> {

}
