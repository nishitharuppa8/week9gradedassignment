package com.hcl.bookess.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.bookess.bean.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, String> {

}