package com.hcl.bookess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week9GradedApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week9GradedApplication.class, args);
	}

}
